package com.example;

/**
 *
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 *
 *
 *
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class TASK1 {
    
    /*Verify if given word is a polindrome.
    This method compare if the first half of a given word is equal to the reversed last half.
    Maybe there is an easier and faster way to do this, like reversing the given 
    word with StringBuilder to compare.*/
    public boolean its_polindrome(String word) {
        //inits
        boolean it_is = false;
        String word_first_half="";
        String word_reversed_last_half = "";
        boolean is_even_word = false;
        
        //get word size
        int word_size = word.length();
        //get word first half
        
        //define stopping criterion to get the reversed last half
        int stopping_criteria = word_size/2;
        if (word_size%2!=0){
             stopping_criteria = word_size/2+1;
        }
        
        //get word first half
        word_first_half = word.substring(0,(int)word_size/2);
        //iteration to get word reversed last half
        for (int i = word_size - 1; i >= stopping_criteria; i--) {
            word_reversed_last_half = word_reversed_last_half + word.charAt(i);
        }
        //if reversed last half is equal the first half
        if (word_reversed_last_half.equals(word_first_half)) {
            //it is a polindrome
            it_is = true;
        }
        
        return it_is;
    }
}
