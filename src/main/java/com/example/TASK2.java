package com.example;

import java.util.List;
import java.util.ListIterator;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 * 
 */
public class TASK2 {
    /*remove middle element from a given list.
    Print the removed value, the previous value and the next value.
    In the end, the list is printed again*/
    public void remove_element_middle(List<String> list){
        //print the original list
        System.out.println("Original list:");
        System.out.println(list);
        //get the idex of the middle element
        int index_middle = list.size()/2;
        //if the list has even size
        if(list.size()%2==0){
            //sub 1 from index_middle
            index_middle -=1;
        }
        //create an interator to iter the list
        ListIterator<String> iterator = list.listIterator();
        //vars
        String current;
        String previous;
        String next;
        //init index that will show us where the iterator is
        int i=0;
        //while iterator hasNext value
        while (iterator.hasNext()){
            //if i == value of the index off the middle element - 1
            if (i == index_middle-1){
                //get the previous value, then go to the next
                previous = iterator.hasNext()? iterator.next():"";
                //get the current value, then go the next
                current = iterator.hasNext()? iterator.next():"";
                //get the next value, the go the next
                next =  iterator.hasNext()? iterator.next():"";
                
                //prints
                System.out.println("Previous element: "+previous);
                System.out.println("Element to be removed: "+current);
                System.out.println("Next element: "+next);
                
                //because the iterator is 2 positions after the element in the middle,
                //it need to go back 2 times to be where we need it to be
                //-postion
                iterator.previous();
                //-position
                iterator.previous();
                //remove the element in the middle of the list
                iterator.remove();
                
                //break the while lasso
                break;
            }
            else{
            //advance the iterator to the next position
            iterator.next();
            //index +1 
            i++;
                    }         
        }
        //here is printed the list, after removal
        System.out.println("List after removal:");
        System.out.println(list);
    }

    
}
