package com.example;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */
public class TASK3 {
    /*This method create a list with random strings contained in alphabet.
    The size must be determined.
    Return created list.
    */
    public List<String> create_list (int list_size){
        //possible letters that will populate the list
        String letters ="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        //init list
        List<String> list = new ArrayList<String>();
        //for lasso to add the strings to the list
        for (int i=0;i<=list_size-1;i++){
            //instance Random class
            Random r = new Random();
            //pick a number beteween 0 and the maximum size of the String letters
            int random = r.nextInt((letters.length()));
            /*here the random number is used to add a random char to the list
            in the process the char value is transformed in String*/
            list.add(String.valueOf(letters.charAt(random)));
        }
        return list;
    }
    /*This method just add to a set the values of a given list, sice the set cant have
    repeated values the output will be the distinct values from the list.
    The same output could be achieved using list stream's method "distinct()":
    //List<Object> distinct_values = list.stream().distinct().collect(Collectors.toList());
    */
    public List<String> distinct_list_items(List<String> list){
        //init output list
        List<String> distinct_values = new ArrayList<String>();
        //init set
        Set<String> s = new HashSet<String>();
        //add the values of the list to the set
        s.addAll(list);
        //add the values of the set to a list
        distinct_values.addAll(s);
        return distinct_values;
    }
    
    /*This method merge the method create list with distinct_list_items.*/
    public void create_random_list_print_distinct (int list_size){
        //init list
        List<String> random_list = new ArrayList<String>();
        //call create list to add random letters to the list
        random_list = create_list(list_size);
        //print list items, list size, distinct values e distinct values size
        System.out.println("Created list:");
        System.out.println(random_list);
        System.out.println("Random list size: " + random_list.size());
        System.out.println("Distinct list values:");
        System.out.println(distinct_list_items(random_list));
        System.out.println("Number of distinct values: " + distinct_list_items(random_list).size());
        
    }
        
}
