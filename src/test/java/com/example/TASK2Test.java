/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.junit.Test;

/**
 *
 * @author holzh
 */
public class TASK2Test {
    
    public TASK2Test() {
    }
    /**
     * Test of remove_element_middle method, of class TASK2.
     */
    @Test
    public void testRemove_element_middle() {
        System.out.println( "Testing remove_element_middle");
        List<String> list = new 
        LinkedList<String>(Arrays.asList("a", "b", "c","d","e","f","g"));

        TASK2 t = new TASK2();
        t.remove_element_middle(list);
        
        //repeat
        t.remove_element_middle(list);
        
        //repeat
        t.remove_element_middle(list);
        
    }
    
}
