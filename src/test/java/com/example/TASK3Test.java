/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
/**
 *
 * @author holzh
 */
public class TASK3Test {
    /**
     * Test of create_list method, of class TASK3.
     */
    
    @Test
    public void testCreate_list() {
        System.out.println("-------------------------------------------------------");
        System.out.println("\n\nTesting create_list");
        TASK3 t = new TASK3();
        int list_size = 20;
        List<String> list_test = new ArrayList<String>();
        
        list_test = t.create_list(list_size);
        System.out.println(list_test);
        assertEquals("Size must be equal",list_size,list_test.size());
        
    }

    /**
     * Test of print_unique_strings method, of class TASK3.
     */
    @Test
    public void testDistinct_list_items() {
        System.out.println("-------------------------------------------------------");
        System.out.println("\n\nTesting distinct_list_items");
        TASK3 t = new TASK3();
        List<String> list_test = Arrays.asList("a", "b", "c","c","c","b","f","g");
        List<String> list_expected = Arrays.asList("a", "b", "c", "f", "g");
        List<String> output_list = new ArrayList<String>();
        
        output_list = t.distinct_list_items(list_test);
        
        assertEquals("Must be equal",list_expected, output_list);
        
    }
    /**
     * Test of create_random_list_print_distinct method, of class TASK3.
     */
    @Test
    public void testCreate_random_list_print_distinct() {
        
        System.out.println("create_random_list_print_distinct");
        
        TASK3 t = new TASK3();
        
        //test with various sizes
        int list_size = 10;
        t.create_random_list_print_distinct(list_size);
        list_size = 5;
        t.create_random_list_print_distinct(list_size);
        list_size = 5;
        t.create_random_list_print_distinct(list_size);
        list_size = 15;
        t.create_random_list_print_distinct(list_size);
        
    }
    
}
