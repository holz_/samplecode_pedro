/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import org.junit.Test;
import static org.junit.Assert.*;
/**
 *
 * @author holzh
 */
public class TASK1Test {
    /**
     * Test of its_polindrome method, of class TASK1.
     * Tests whether given words are polindrome or not
     */
    
    @Test
    public void testIts_polindrome() {
        System.out.println("Testing its_polindrome");
        TASK1 t = new TASK1();
        
        String word = "racecar";
        assertTrue("must be true",t.its_polindrome(word));
        
        word = "house";
        assertFalse("must be false",t.its_polindrome(word));
        
        word = "caaaac";
        assertTrue("must be true",t.its_polindrome(word));
        
        word = "caac";
        assertTrue("must be true",t.its_polindrome(word));
        
        word = "last";
        assertFalse("must be false",t.its_polindrome(word));
    }
    
}
